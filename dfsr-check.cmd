@echo off

set smem=nimbus-%1
set rmem=nimbus-%2

for %%g in (WebFTP,Clients,geoweather,Installers,ObsQuesnel,Radar,web,SLA,site,ProductAssets,ProductsOutput,Shape,SpectralWaveData,Tiles) do (
  echo Folder: %%g
  dfsrdiag backlog /RGName:file-system /RFName:%%g /Smem:%smem% /Rmem:%rmem% | findstr Count
  echo.
)