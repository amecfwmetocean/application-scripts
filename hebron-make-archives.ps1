param (
  [DateTime]$baseDate = ([DateTime]::Now)
)

function create-7zip([String] $aDirectory, [String] $aZipfile){
  [string]$pathToZipExe = "C:\Program Files\7-zip\7z.exe";
  [Array]$arguments = "a", "-tzip", "$aZipfile", "$aDirectory", "-r";
  & $pathToZipExe $arguments;
}

$archiveRoot = "s3://nimbus-clients-archive/Hebron"
$scratchDir = "X:\Temp\hebron\"
[Array]$siteIDs = "ANLTH", "BUOY", "TGMS"
[Array]$forecastIDs = "MNLBMC", "ROADCOND"

function archive-site([string]$siteId, [string]$prefix = "", [string]$filter = "*") {
  rm $scratchDir -Recurse -Force
  mkdir $scratchDir

  for ($i = 1; $i -lt 8; $i++) {
    $date = $baseDate.AddDays(-$i)
    $s3Path = "{0}/{1}{2}/{3:yyyy/MM/dd}/" -f $archiveRoot,$siteID,$prefix,$date
    aws s3 cp $s3Path $scratchDir --recursive --exclude * --include $filter
  }

  if (Test-Path $($scratchDir + "*")) {
    $zipFile = "X:\Temp\{0}_{1:yyyy-MM-dd}_Weekly.zip" -f $siteID,$baseDate.AddDays(-1)
    create-7zip $($scratchDir + "*.*")  $zipFile
    $s3ZipPath = "{0}/{1}{2}/{3:yyyy}/" -f $archiveRoot,$siteID,$prefix,$baseDate.AddDays(-1)
    aws s3 mv $zipFile $s3ZipPath
  }
}

foreach ($siteID in $siteIDs) {
  archive-site $siteID
}

foreach ($siteID in $forecastIDs) {
  archive-site $siteID "/Products" "*.pdf"
}