@echo off

set blat=C:\Applications\blat\blat.exe

aws s3 ls s3://nimbus-ftp/client-ans/ --recursive | find /I /V ".jpg" > ans.txt
type ans.txt | find /C "2014" > anscount.txt
set /p anscount= < anscount.txt

if %anscount% gtr 30 (
  %blat% ans.txt -to lloyd.cotten@amec.com,lloyd+sms@lloydcotten.com -f sys-notify@amecweather.net -server nimbus-smtp -subject "Obs backlog discovered for client-ans"
::  net stop "UOM Sink"
::  timeout 10
::  net start "UOM Sink"
)

del ans.txt
del anscount.txt

aws s3 ls s3://nimbus-ftp/amec-polling/ --recursive | find /I /V ".jpg" | find /V ".log" | find /V ".tar.gz" | find /V "MB.MIT." > amec.txt
type amec.txt | find /C "2014" > ameccount.txt
set /p ameccount= < ameccount.txt

if %ameccount% gtr 50 (
  %blat% amec.txt -to lloyd.cotten@amec.com,lloyd+sms@lloydcotten.com -f sys-notify@amecweather.net -server nimbus-smtp -subject "Obs backlog discovered for amec-polling"
  net stop "UOM Sink"
  timeout 10
  net start "UOM Sink"
)

del amec.txt
del ameccount.txt

aws s3 ls s3://nimbus-ftp/client-mbdot/ --recursive | find /I /V ".jpg" > mb.txt
type mb.txt | find /C "2014" > mbcount.txt
set /p mbcount= < mbcount.txt

if %mbcount% gtr 15 (
  %blat% mb.txt -to lloyd.cotten@amec.com,lloyd+sms@lloydcotten.com -f sys-notify@amecweather.net -server nimbus-smtp -subject "Obs backlog discovered for client-mbdot"
  net stop "UOM Sink"
  timeout 10
  net start "UOM Sink"
)

del mb.txt
del mbcount.txt