param (
  [DateTime]$baseDate = ([DateTime]::Now)
)

function create-7zip([String] $aDirectory, [String] $aZipfile){
  [string]$pathToZipExe = "C:\Program Files\7-zip\7z.exe";
  [Array]$arguments = "a", "-tzip", "$aZipfile", "$aDirectory", "-r";
  & $pathToZipExe $arguments;
}

$archiveRoot = "s3://nimbus-ftp/amec-polling/NL/HMDC"
$archiveFolder = "Archive"
$scratchDir = "X:\Temp\hmdc\"
[Array]$subFolders = "BUOY"

function archive-site([string]$folder, [string]$filter = "*") {
  rm $scratchDir -Recurse -Force
  mkdir $scratchDir

  for ($i = 1; $i -lt 8; $i++) {
    $date = $baseDate.AddDays(-$i)
    $s3Path = "{0}/{1}" -f $archiveRoot,$folder
    aws s3 mv $s3Path $scratchDir --recursive --exclude * --include $filter
  }

  if (Test-Path $($scratchDir + "*")) {
    $zipFile = "X:\Temp\{0}_{1:yyyy-MM-dd}_Monthly.zip" -f $folder,$baseDate.AddDays(-1)
    create-7zip $($scratchDir + "*.*")  $zipFile
	
    $s3ZipPath = "{0}/{1}/" -f $archiveRoot,$archiveFolder
    aws s3 mv $zipFile $s3ZipPath
  }
}

foreach ($folder in $subFolders) {
  archive-site $folder "*.log"
}