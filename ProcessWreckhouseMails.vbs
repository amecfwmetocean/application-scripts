Sub HandleWreckhouseMessage(statusFile)
    dim contents, objReadStatus
    contents = ""
    prevContents = ""

    if statusFile.Size > 0 then

        Set objReadStatus = statusFile.OpenAsTextStream(1)
        contents = objReadStatus.ReadAll
        objReadStatus.Close
        Set objReadStatus = Nothing    

        if InStr(contents, "100 KM/H") > 0 then
            Publish "wreckhouse-sign-exceedes-100-kmh.jpg"
        elseif InStr(contents, "80 KM/H") > 0 then
            Publish "wreckhouse-sign-exceedes-80-kmh.jpg"
        elseif InStr(contents, "60 KM/H") > 0 then
            Publish "wreckhouse-sign-exceedes-60-kmh.jpg"
        else
            Publish "wreckhouse-sign-default.jpg"
        end if
    end if
End Sub

Sub Publish(sourceFile)
    Dim exec, sourceDir, destDir, destFile, ltext, rtext, params
    Dim wshShell

    exec = "C:\Applications\AwxImaging\image-util.exe"
    sourceDir = "\\nimbus-app\awx\ProductAssets\"
    destDir = "\\nimbus-app\awx\ProductsOutput\"
    destFile = "S1.NL.NLDOT.RNLWK." & DateTimeYYYYMMDDTHHMMSSZ() & ".jpg"
    ltext = "Wreckhouse NL CA"
    rtext = "{ts:NST:ddd MMM d yyyy h:mm:ss tt}"
    params = " -c banner -i " & sourceDir & sourceFile & _
        " -o " & destDir & destFile & _
        " --bgcolor transparent --fgcolor black -h 16" &  _
        " --ltext """ & ltext & """ --rtext """ & rtext & """"
    

    Set wshShell = WScript.CreateObject("WScript.Shell")
    wshShell.Run exec & params, 0, true
    wshShell.Run "aws s3 mv " & destDir & destFile & " s3://nimbus-products/out/", 0, true
End Sub

Function DateTimeYYYYMMDDTHHMMSSZ()
    Dim retv, d
    d = Now
    retv = Year(d) & Right("00" & Month(d), 2) & Right("00" & Day(d), 2) & "T" & Right("00" & Hour(d), 2) & Right("00" & Minute(d), 2) & Right("00" & Second(d), 2) & "Z"
    DateTimeYYYYMMDDTHHMMSSZ = retv
End Function

dim m_objFSO, m_obsFolder, m_objStatusFile
m_obsFolder = "M:\Wreckhouse\in"
Set m_objFSO = CreateObject("Scripting.FileSystemObject")

if not m_objFSO.FileExists(m_obsFolder & "\status.txt") then
    Publish "wreckhouse-sign-default.jpg"
else
    Set m_objStatusFile = m_objFSO.GetFile(m_obsFolder & "\status.txt")
    if DateDiff("n", m_objStatusFile.DateLastModified, Now) < 2 then
        HandleWreckhouseMessage m_objStatusFile
    end if
end if 

Set m_objStatusFile = Nothing
Set m_objFSO = Nothing

wscript.quit