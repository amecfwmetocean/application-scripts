Option Explicit

Sub HandleObservationsMessage(path)
    Dim objFSO, objReadFile, contents, lines
    Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	'Check for 0 Byte files, as this will cause problems when reading the file	
	If objFSO.GetFile(path).Size = 0 Then 'Don't process empty files, just delete them
		Set objFSO = Nothing
		Exit Sub
	End If

	Set objReadFile = objFSO.OpenTextFile(path, 1, False)
    contents = objReadFile.ReadAll
    objReadFile.close

    lines = Split(contents, vbLf)

    Dim line, obs, obsType
    obs = ""
    obsType = ""

	' Intercept the Fugro feed files and move them elsewhere for pick-up by UOM
	if Instr(lines(0), "hm0,1024_sec") = 1 then
		Dim fname
		fname = objFSO.GetFileName(path)
		objFSO.CopyFile path, "M:\Observations\fugro\" & fname
	end if
	
	Set objReadFile = Nothing
    Set objFSO = Nothing
	
    For Each line in lines
	
        line = Replace(Trim(line), vbCr, "")
		' This is a workaround for one of Exxons METAR observation files. Their automated system neglects to use METAR at the start of the message. and its only for their TCF station
		if Instr(line, "TCF") = 1 then
			line = "METAR " & line
		end if

        if Instr(line, "METAR") = 1 or Instr(line, "SPECI") = 1 then
			if obsType <> "" and obs <> "" then
                PublishObs obs, obsType
            end if
            obs = line
            obsType = "METAR"
        elseif Instr(line, "BBXX") > 0 then
            if obsType <> "" and obs <> "" then
                PublishObs obs, obsType
            end if            
            obs = line
            obsType = "MANMAR"
        elseif line = "" then
            if obsType <> "" and obs <> "" then
                PublishObs obs, obsType
            end if
            obs = ""
            obsType = ""
        else
            obs = Trim(obs) & " " & Trim(line)
        end if
    Next 
End Sub

Sub PublishObs(obs, obsType)
    obs = Replace(obs, "=", "") & "="
    if obsType = "MANMAR" then
        WriteManmar(obs)
    else
        WriteMetar(obs)
    end if
End Sub

Sub WriteManmar(obs)
    Dim startIndex, prefix, groups, dt
    
    startIndex = InStr(obs, "BBXX")
    prefix = Trim(Mid(obs, 1, startIndex - 1))
    obs = Mid(obs, startIndex)
    SendManmar(obs)

    groups = Split(obs, " ")
    dt = Mid(groups(2), 1, 4) & "00"
    obs = "SMVD03 CWAO " & dt & vbCrLf & "BBXX" & vbCrLf & Mid(obs, 6)

    Dim objFSO, objFile
    Dim strDirectory, strFile
    strDirectory = "M:\Observations\"
    strFile = "SMVD03_" & Replace(prefix, " ", "_") & groups(1) & ".manmar.txt"

    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set objFile = objFSO.CreateTextFile(strDirectory & strFile, true)
    objFile.Write(obs)
    objFile.Close
End Sub

Sub WriteMetar(obs)
    Dim startIndex, prefix, parts

    startIndex = InStr(obs, "Z")
	if startIndex > 0 then
		prefix = Trim(Mid(obs, 1, startIndex))
    
		parts = Split(prefix, " ")
		obs = "SACN31 CWAO " & Trim(Replace(parts(2), "Z", "")) & vbCrLf & obs

		Dim objFSO, objFile
		Dim strDirectory, strFile
		strDirectory = "M:\Observations\"
		strFile = "SACN31_" & Replace(prefix, " ", "_") & parts(1) & ".metar.txt"
	
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		Set objFile = objFSO.CreateTextFile(strDirectory & strFile, true)
		objFile.Write(obs)
		objFile.Close
	end if
End Sub

Sub SendManmar(obs)
    Dim objWMIService, objStartup, objConfig, objProcess
    Dim strComputer, strExe, intProcessID, intReturn

    strComputer = "."
    strExe = "C:\Applications\blat\blat.exe -to ""vos@ec.gc.ca"" -body """ & obs & """ " & _
        "-f observations@amecweather.net -subject MANMAR -server 172.16.5.26"

    ' Connect to WMI
    set objWMIService = getobject("winmgmts://" & strComputer & "/root/cimv2") 
    Set objStartup = objWMIService.Get("Win32_ProcessStartup")
    Set objConfig = objStartup.SpawnInstance_
    objConfig.ShowWindow = 0

    Set objProcess = objWMIService.Get("Win32_Process")
    intReturn = objProcess.Create(strExe, Null, objConfig, intProcessID)
End Sub


dim m_objFSO, m_obsFolder, m_objFolder, m_colFiles, m_objFile
m_obsFolder = "M:\Observations\in"
Set m_objFSO = CreateObject("Scripting.FileSystemObject")
Set m_objFolder = m_objFSO.GetFolder(m_obsFolder)

Set m_colFiles = m_objFolder.Files
For Each m_objFile in m_colFiles
    HandleObservationsMessage(m_objFile.Path)
    m_objFSO.DeleteFile(m_objFile.Path)
Next

Set m_objFolder = Nothing
Set m_objFSO = Nothing

wscript.quit